import { JsonCustomConvert } from "json2typescript";
declare class EnumConvertorBase<T> implements JsonCustomConvert<T> {
    protected enumType: unknown;
    protected enumName: string;
    validValues: string[];
    constructor(enumType: unknown, enumName: string);
    deserialize(_value: string | string[] | null): T;
    serialize(data: T): any;
}
export declare class EnumConvertor<T> extends EnumConvertorBase<T> {
    deserialize(value: string | null): T;
}
export declare class EnumArrayConvertor<T> extends EnumConvertorBase<T> {
    deserialize(value: string[] | null): T;
}
export {};
