"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnumArrayConvertor = exports.EnumConvertor = void 0;
var EnumConvertorBase = /** @class */ (function () {
    function EnumConvertorBase(enumType, enumName) {
        this.enumType = enumType;
        this.enumName = enumName;
        this.validValues = Object.values(this.enumType).map(function (value) { return value; });
    }
    EnumConvertorBase.prototype.deserialize = function (_value) {
        throw new Error("Enum deserialisation error");
    };
    EnumConvertorBase.prototype.serialize = function (data) {
        return data;
    };
    return EnumConvertorBase;
}());
var EnumConvertor = /** @class */ (function (_super) {
    __extends(EnumConvertor, _super);
    function EnumConvertor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EnumConvertor.prototype.deserialize = function (value) {
        if ((typeof value !== "string" && value !== null) ||
            (typeof value === "string" &&
                value !== null &&
                !this.validValues.includes(value))) {
            throw new Error("JsonConvert error; invalid value for enum " + this.enumName + ", expected one of '" + this.validValues + "', found '" + value + "'");
        }
        return value;
    };
    return EnumConvertor;
}(EnumConvertorBase));
exports.EnumConvertor = EnumConvertor;
var EnumArrayConvertor = /** @class */ (function (_super) {
    __extends(EnumArrayConvertor, _super);
    function EnumArrayConvertor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EnumArrayConvertor.prototype.deserialize = function (value) {
        var _this = this;
        if ((!Array.isArray(value) && value !== null) ||
            (Array.isArray(value) &&
                value !== null &&
                !(value === null || value === void 0 ? void 0 : value.every(function (item) { return _this.validValues.includes(item); })))) {
            throw new Error("JsonConvert error; invalid value for enum " + this.enumName + ", expected one of '" + this.validValues + "', found '" + value + "'");
        }
        return value;
    };
    return EnumArrayConvertor;
}(EnumConvertorBase));
exports.EnumArrayConvertor = EnumArrayConvertor;
//# sourceMappingURL=EnumConverter.js.map