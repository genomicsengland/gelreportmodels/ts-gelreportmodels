"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var AcmgVariantClassification_1 = require("../AcmgVariantClassification");
var acmgVariantClassification = {
    assessment: null,
    noAcmgEvidence: null,
    clinicalSignificance: "test-1-clinical-significance",
    acmgEvidences: [
        {
            category: "test-1-category",
            type: "test-1-type",
            weight: "test-1-weight",
            modifier: 1,
            activationStrength: "test-1-activation-strength",
            description: "test-1-activation-description",
            comments: [{ comment: "test-1-activation-description" }],
        },
    ],
};
describe("AcmgVariantClassification", function () {
    it("Should parse when the `noAcmgEvidence` field is null", function () {
        AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(acmgVariantClassification);
    });
    it("Should parse when the `noAcmgEvidence` field is undefined", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: undefined });
        AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    });
    it("Should parse when the `noAcmgEvidence` field contains an array of valid enums (single item)", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: ["population_data"] });
        AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    });
    it("Should parse when the `noAcmgEvidence` field contains an array of valid enums (multiple item)", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: [
                "population_data",
                "computational_and_predictive_data",
                "de_novo_data",
            ] });
        AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    });
    it("Should not parse when the `noAcmgEvidence` field contains a string", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: "population_data" });
        expect(function () {
            AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
        }).toThrowError();
    });
    it("Should not parse when the `noAcmgEvidence` field contains an array of errant values", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: ["invalid", "strings"] });
        expect(function () {
            AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
        }).toThrowError();
    });
    it("Should not parse when the `noAcmgEvidence` field contains an array of partially errant values", function () {
        var updatedAcmgVariantClassification = __assign(__assign({}, acmgVariantClassification), { noAcmgEvidence: ["invalid", "strings", "population_data"] });
        expect(function () {
            AcmgVariantClassification_1.AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
        }).toThrowError();
    });
});
//# sourceMappingURL=AcmgVariantClassification.test.js.map