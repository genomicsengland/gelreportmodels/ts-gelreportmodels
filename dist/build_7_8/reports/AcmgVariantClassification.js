"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AcmgVariantClassification = void 0;
var JSONHelper_1 = __importDefault(require("../../utils/JSONHelper"));
var AcmgEvidence_1 = require("./AcmgEvidence");
var AcmgEvidenceCategory_1 = require("./AcmgEvidenceCategory");
var json2typescript_1 = require("json2typescript");
/**
 * Full record for the ACMG variant classification, including all selected
 * evidences and the final classification.
 */
var AcmgVariantClassification = /** @class */ (function (_super) {
    __extends(AcmgVariantClassification, _super);
    function AcmgVariantClassification() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Details of ACMG criteria used to score this variant
         */
        _this.acmgEvidences = [];
        /**
         * Final classification selected by user
         */
        _this.clinicalSignificance = ""; // ClinicalSignificance
        /**
         * Classification computed from ACMG scores
         */
        _this.assesment = undefined;
        /**
         * ACMG evidence categories for which the user has indicated there is no evidence available
         */
        _this.noAcmgEvidence = undefined;
        return _this;
    }
    __decorate([
        json2typescript_1.JsonProperty("acmgEvidences", [AcmgEvidence_1.AcmgEvidence])
    ], AcmgVariantClassification.prototype, "acmgEvidences", void 0);
    __decorate([
        json2typescript_1.JsonProperty("clinicalSignificance", String)
    ], AcmgVariantClassification.prototype, "clinicalSignificance", void 0);
    __decorate([
        json2typescript_1.JsonProperty("assesment", String, true)
    ], AcmgVariantClassification.prototype, "assesment", void 0);
    __decorate([
        json2typescript_1.JsonProperty("noAcmgEvidence", AcmgEvidenceCategory_1.AcmgEvidenceCategoryConvertor, true)
    ], AcmgVariantClassification.prototype, "noAcmgEvidence", void 0);
    AcmgVariantClassification = __decorate([
        json2typescript_1.JsonObject
    ], AcmgVariantClassification);
    return AcmgVariantClassification;
}(JSONHelper_1.default));
exports.AcmgVariantClassification = AcmgVariantClassification;
//# sourceMappingURL=AcmgVariantClassification.js.map