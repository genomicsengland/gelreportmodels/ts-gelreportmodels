"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AcmgEvidenceCategoryConvertor = exports.AcmgEvidenceCategory = void 0;
var json2typescript_1 = require("json2typescript");
var EnumConverter_1 = require("../../utils/EnumConverter");
/**
 * Each ACMG criterion is classified in one of these categories
 */
var AcmgEvidenceCategory;
(function (AcmgEvidenceCategory) {
    AcmgEvidenceCategory["population_data"] = "population_data";
    AcmgEvidenceCategory["computational_and_predictive_data"] = "computational_and_predictive_data";
    AcmgEvidenceCategory["functional_data"] = "functional_data";
    AcmgEvidenceCategory["segregation_data"] = "segregation_data";
    AcmgEvidenceCategory["de_novo_data"] = "de_novo_data";
    AcmgEvidenceCategory["allelic_data"] = "allelic_data";
    AcmgEvidenceCategory["other_database"] = "other_database";
    AcmgEvidenceCategory["other_data"] = "other_data";
})(AcmgEvidenceCategory = exports.AcmgEvidenceCategory || (exports.AcmgEvidenceCategory = {}));
var AcmgEvidenceCategoryConvertor = /** @class */ (function (_super) {
    __extends(AcmgEvidenceCategoryConvertor, _super);
    function AcmgEvidenceCategoryConvertor() {
        return _super.call(this, AcmgEvidenceCategory, "AcmgEvidenceCategoryArray") || this;
    }
    AcmgEvidenceCategoryConvertor = __decorate([
        json2typescript_1.JsonConverter
    ], AcmgEvidenceCategoryConvertor);
    return AcmgEvidenceCategoryConvertor;
}(EnumConverter_1.EnumArrayConvertor));
exports.AcmgEvidenceCategoryConvertor = AcmgEvidenceCategoryConvertor;
//# sourceMappingURL=AcmgEvidenceCategory.js.map