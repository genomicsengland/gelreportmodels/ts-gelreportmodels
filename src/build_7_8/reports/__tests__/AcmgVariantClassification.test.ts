import { AcmgVariantClassification } from "../AcmgVariantClassification";

const acmgVariantClassification = {
  assessment: null,
  noAcmgEvidence: null,
  clinicalSignificance: "test-1-clinical-significance",
  acmgEvidences: [
    {
      category: "test-1-category",
      type: "test-1-type",
      weight: "test-1-weight",
      modifier: 1,
      activationStrength: "test-1-activation-strength",
      description: "test-1-activation-description",
      comments: [{ comment: "test-1-activation-description" }],
    },
  ],
};

describe("AcmgVariantClassification", () => {
  it("Should parse when the `noAcmgEvidence` field is null", () => {
    AcmgVariantClassification.fromJSON(acmgVariantClassification);
  });

  it("Should parse when the `noAcmgEvidence` field is undefined", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: undefined,
    };

    AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
  });

  it("Should parse when the `noAcmgEvidence` field contains an array of valid enums (single item)", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: ["population_data"],
    };

    AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
  });

  it("Should parse when the `noAcmgEvidence` field contains an array of valid enums (multiple item)", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: [
        "population_data",
        "computational_and_predictive_data",
        "de_novo_data",
      ],
    };

    AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
  });

  it("Should not parse when the `noAcmgEvidence` field contains a string", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: "population_data",
    };

    expect(() => {
      AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    }).toThrowError();
  });

  it("Should not parse when the `noAcmgEvidence` field contains an array of errant values", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: ["invalid", "strings"],
    };

    expect(() => {
      AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    }).toThrowError();
  });

  it("Should not parse when the `noAcmgEvidence` field contains an array of partially errant values", () => {
    const updatedAcmgVariantClassification = {
      ...acmgVariantClassification,
      noAcmgEvidence: ["invalid", "strings", "population_data"],
    };

    expect(() => {
      AcmgVariantClassification.fromJSON(updatedAcmgVariantClassification);
    }).toThrowError();
  });
});
