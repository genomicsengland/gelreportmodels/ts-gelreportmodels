import { JsonConverter } from "json2typescript";
import { EnumArrayConvertor } from "../../utils/EnumConverter";

/**
 * Each ACMG criterion is classified in one of these categories
 */
export enum AcmgEvidenceCategory {
  population_data = "population_data",
  computational_and_predictive_data = "computational_and_predictive_data",
  functional_data = "functional_data",
  segregation_data = "segregation_data",
  de_novo_data = "de_novo_data",
  allelic_data = "allelic_data",
  other_database = "other_database",
  other_data = "other_data",
}

@JsonConverter
export class AcmgEvidenceCategoryConvertor extends EnumArrayConvertor<AcmgEvidenceCategory> {
  constructor() {
    super(AcmgEvidenceCategory, "AcmgEvidenceCategoryArray");
  }
}
