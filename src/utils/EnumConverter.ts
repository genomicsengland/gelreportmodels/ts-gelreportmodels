import { JsonCustomConvert } from "json2typescript";

class EnumConvertorBase<T> implements JsonCustomConvert<T> {
  validValues: string[];

  constructor(protected enumType: unknown, protected enumName: string) {
    this.validValues = Object.values(this.enumType as object).map(
      (value) => value
    );
  }

  deserialize(_value: string | string[] | null): T {
    throw new Error("Enum deserialisation error");
  }

  serialize(data: T): any {
    return data;
  }
}

export class EnumConvertor<T> extends EnumConvertorBase<T> {
  deserialize(value: string | null): T {
    if (
      (typeof value !== "string" && value !== null) ||
      (typeof value === "string" &&
        value !== null &&
        !this.validValues.includes(value))
    ) {
      throw new Error(
        `JsonConvert error; invalid value for enum ${this.enumName}, expected one of '${this.validValues}', found '${value}'`
      );
    }

    return value as unknown as T;
  }
}

export class EnumArrayConvertor<T> extends EnumConvertorBase<T> {
  deserialize(value: string[] | null): T {
    if (
      (!Array.isArray(value) && value !== null) ||
      (Array.isArray(value) &&
        value !== null &&
        !value?.every((item) => this.validValues.includes(item)))
    ) {
      throw new Error(
        `JsonConvert error; invalid value for enum ${this.enumName}, expected one of '${this.validValues}', found '${value}'`
      );
    }

    return value as unknown as T;
  }
}
